var express = require('express');
var auth = require('basic-auth');
var bodyParser = require('body-parser');

var app = express();
var users = require('./data/users.json');
var tickets = [];

function sendResponse(res, data, error, status) {
    if (error) {
        res.status(status || 500).json({
            error: error
        });
    } else {
        res.json({
            data: data
        });
    }
}

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'get, post, delete, put');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization');
    next();
});
app.use(function (req, res, next) {
    var user = auth(req);

    if (req.method === 'OPTIONS' || (user && user.name === 'admin' && user.pass === 'admin')) {
        next();
    } else {
        console.log('Unauthorized');
        res.sendStatus(401);
    }
});

app.get('/me', function (req, res) {
    console.log('GET /me');
    sendResponse(res, {
        firstname: 'Super',
        lastname: 'Admin'
    });
});

app.get('/users', function (req, res) {
    console.log('GET /users');
    sendResponse(res, users);
});

app.get('/users/:userId', function (req, res) {
    console.log('GET /users/:userId', req.params.userId);
    var byId = users.filter(function (user) {
        return user.id === parseInt(req.params.userId);
    });
    if (byId.length) {
        sendResponse(res, byId[0]);
    } else {
        res.sendStatus(404);
    }
});

app.get('/users/:userId/tickets', function (req, res) {
    console.log('GET /users/:userId/tickets', req.params.userId);
    var userId = parseInt(req.params.userId);

    var userTickets = tickets.filter(function (ticket) {
        return ticket.userId === userId;
    });

    sendResponse(res, userTickets);
});

app.post('/users/:userId/tickets', function (req, res) {
    console.log('POST /users/:userId/tickets', req.body);
    if (!req.body.description) {
        return sendResponse(res, null, "Ticket must have description", 400);
    }

    var userId = parseInt(req.params.userId);
    var newId = tickets.length + 1;

    tickets.push({
        id: newId,
        userId: userId,
        description: req.body.description,
        timeCreated: +(new Date())
    });

    sendResponse(res, {
        id: newId
    });
});

app.delete('/tickets/:ticketId', function (req, res) {
    console.log('DELETE /ticket/:ticketId', req.params.ticketId);
    var ticketId = parseInt(req.params.ticketId, 10);
    var ticketsNum = tickets.length;

    for (var i = 0; i < ticketsNum; i++) {
        if (tickets[i].id === ticketId) {
            tickets.splice(i, 1);
            return res.sendStatus(200);
        }
    }

    res.sendStatus(404);
});

app.listen(process.env.PORT || 8000);
